const path = require('path')
const Htmlwebpackplugin = require('html-webpack-plugin')

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '/build'),
    filename: 'index.bundle.js',
    clean: true,
    publicPath: '/'
  },
  devServer: {
    port: 3000,
    static: path.resolve(__dirname, 'src'),
    historyApiFallback: true,
    host: '0.0.0.0',
  },
  module: {
    rules: [
      {
        test: /\.js/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          babelrc: true,
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          {loader: 'css-loader'},
          {loader: 'sass-loader'},
        ],
      },
    ]
  },
  plugins: [
    new Htmlwebpackplugin({
      template: './public/index.html'
    })
  ]
}