import React from 'react'
import ReactDom from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'
import Aplication from './components/Aplication'
import './index.scss'

const root = ReactDom.createRoot(document.getElementById('root'))

root.render(
	<Router>
    
		<Aplication />
	</Router>
)
